import React, { Component } from "react";
export default class FormTodo extends Component {

  state = {
    inputValue: "",
    todos: [],
  };

  inputChange = (e) => {
    this.setState({
      inputValue: e.target.value,
    });
  }

  handleSubmit = (e) => {
    e.preventDefault()
    this.setState({
      todos: [...this.state.todos, this.state.inputValue],
      inputValue: "",
    });
  };



  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <input type="text" value={this.state.inputValue} placeholder="Enter task..." onChange={this.inputChange} />
          <button type="submit">Add task</button>
        </form>
        <ul>
          {this.state.todos.map((todo) => (
            <li>{todo}</li>
          ))}
        </ul>
      </div>
    );
  }
}
