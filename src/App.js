import './App.css';
import React from 'react';
import FormTodo from './component/Tasklist';



function App() {
  return (

    <div className="App">
      <h1>Task app</h1>
      <FormTodo />
    </div>
  );
}


export default App;
